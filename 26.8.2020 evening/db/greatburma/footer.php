<section class="return-to-top-section pad-0"><a class="return-to-top btn-floating waves-effect waves-light" href="javascript:"><i class="fas fa-arrow-up"> </i></a></section> 
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4 email">
          <div class="footer-logo"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/ft-logo.png" alt="">
            <div class="logo-text">
              <h3>Great Memories</h3>
              <p>travel & tour </p>
            </div>
          </div>
          <div class="share-with-us-blog"><a class="text-center" href="#"><i class="fab fa-facebook-f"></i></a><a class="text-center" href="#"><i class="fab fa-twitter"></i></a></div>
        </div>
        <div class="col-md-4 address">
          <h3>Links</h3>
          <ul>
            <li> <a href="index.html">Home</a></li>
            <li> <a href="about.html">About </a></li>
            <li> <a href="tour-packages.html">Tour Packages </a></li>
            <li> <a href="customise-tour.html">Customise Tour </a></li>
            <li> <a href="contactus.html"> Contact </a></li>
          </ul>
        </div>
        <div class="col-md-4 ph-number">
          <h3>Contact Us </h3>
          <div class="contact-phone d-flex">
            <p class="bold">Phone</p><a href="tel:+95 9 xxxxxxxxx">+95 9 xxxxxxxxx</a>
          </div>
          <div class="contact-mail d-flex">
            <p class="bold">Email </p><a href="mailto:">xxxxxx @xxxx.com</a>
          </div>
          <div class="contact-address d-flex">
            <p class="bold">Address</p><span class="address-txt">xxxxx xxxx xxx xxxxxxxxxx xxxxxxxxxxx xxxxxx</span>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <nav class="copy-right">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <p>Copyright &copy; <script>
              new Date().getFullYear() > 2010 && document.write(new Date().getFullYear());
            </script> Great Memories Travel & Tours Co.,Ltd. All Rights Reserved. Powered by<a href="http://www.innovixdigital.com/" target="_blank">Innovix Digital</a></p>
        </div>
      </div>
    </div>
  </nav>
</body>
<script src="<?php bloginfo('template_url'); ?>/js/core.min.js"></script>
<script src="https://kit.fontawesome.com/0fdd460947.js" crossorigin="anonymous"></script>

      
      


</html>