<?php
    get_header();
    get_template_part('otherheader');
    if(have_posts()):
    while(have_posts()):
        the_post(); 
        
        endwhile;
    endif;
?>
<section class="contact-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="contact-info">
          <h3 class="bold section-header header-underline">Contact Information</h3>
          <ul>
            <li><i class="fas fa-map-marked-alt"></i>
              <p><?= get_field("address") ?></p>
            </li>
            <li><i class="fas fa-phone-alt"></i>
              <p><?= get_field("phone_1") ?>,<?= get_field("phone_2") ?></p>
            </li>
            <li><i class="fas fa-envelope"></i>
              <p><?= get_field("email") ?></p>
            </li>
          </ul>
        </div>
        <div class="share-with-us">
          <h5 class="bold">Socially Connect</h5>
          <div class="share-with-us-blog">
            <a href="<?= get_field("facebook") ?>">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a href="<?= get_field("you_tube") ?>">
              <i class="fab fa-youtube"></i>
            </a>
            <a href="<?= get_field("linkedin") ?>">
              <i class="fab fa-linkedin-in"></i>
            </a>
            
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="map-wrap"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3819.0750036298964!2d96.12912815072063!3d16.822634823216944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c195a72ebbce3f%3A0xedb26d5ff07c1132!2sInnovix+Solutions!5e0!3m2!1sen!2smm!4v1565758307927!5m2!1sen!2smm" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe></div>
      </div>
    </div>
  </div>
</section>
<?php     
    get_footer();
?>