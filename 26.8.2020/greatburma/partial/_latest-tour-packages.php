<section class="latest-tours">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-header header-underline text-center">
            <h3 class="bold">Latest Tour Packages</h3>
          </div>
        </div>
      </div>
      <div class="row tour-card-wrapper">
        <div class="col-md-6 tour-card">
          <div class="card"><a href="tour-packages-detail.html">
              <div class="view overlay"><img class="card-img-top w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/tour1.png" alt="banner one">
                <div class="mask rgba-white-slight"> </div>
              </div>
              <div class="card-body">
                <h4 class="bold">Tour Package Name</h4>
                <div class="card-text">
                  <div class="duration"><i class="fas fa-history"></i>
                    <p>3 Days 2 Nights</p>
                  </div>
                  <div class="price">
                    <h4>KS 200,000</h4>
                  </div>
                </div>
                <div class="button-read text-right">
                  <p class="upper bold">Read More</p>
                </div>
              </div>
            </a></div>
        </div>
        <div class="col-md-6 tour-card">
          <div class="card"><a href="tour-packages-detail.html">
              <div class="view overlay"><img class="card-img-top w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/tour2.png" alt="banner one">
                <div class="mask rgba-white-slight"> </div>
              </div>
              <div class="card-body">
                <h4 class="bold">Tour Package Name</h4>
                <div class="card-text">
                  <div class="duration"><i class="fas fa-history"></i>
                    <p>3 Days 2 Nights</p>
                  </div>
                  <div class="price">
                    <h4>KS 200,000</h4>
                  </div>
                </div>
                <div class="button-read text-right">
                  <p class="upper bold">Read More </p>
                </div>
              </div>
            </a></div>
        </div>
        <div class="col-md-6 tour-card">
          <div class="card"><a href="tour-packages-detail.html">
              <div class="view overlay"><img class="card-img-top w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/tour3.png" alt="banner one">
                <div class="mask rgba-white-slight"> </div>
              </div>
              <div class="card-body">
                <h4 class="bold">Tour Package Name</h4>
                <div class="card-text">
                  <div class="duration"><i class="fas fa-history"></i>
                    <p>3 Days 2 Nights</p>
                  </div>
                  <div class="price">
                    <h4>KS 200,000</h4>
                  </div>
                </div>
                <div class="button-read text-right">
                  <p class="upper bold">Read More</p>
                </div>
              </div>
            </a></div>
        </div>
        <div class="col-md-6 tour-card">
          <div class="card"><a href="tour-packages-detail.html">
              <div class="view overlay"><img class="card-img-top w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/tour4.png" alt="banner one">
                <div class="mask rgba-white-slight"> </div>
              </div>
              <div class="card-body">
                <h4 class="bold">Tour Package Name</h4>
                <div class="card-text">
                  <div class="duration"><i class="fas fa-history"></i>
                    <p>3 Days 2 Nights</p>
                  </div>
                  <div class="price">
                    <h4>KS 200,000</h4>
                  </div>
                </div>
                <div class="button-read text-right">
                  <p class="upper bold">Read More</p>
                </div>
              </div>
            </a></div>
        </div>
      </div>
    </div>
  </section>