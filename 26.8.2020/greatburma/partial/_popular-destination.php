<?php
    
    
    $query = new WP_Query('pagename=popular-destination');
    if($query->have_posts()):
    while($query->have_posts()):
      $query->the_post(); 
      $destination_image_1 = get_field("destination_image_1");
      $destination_image_2 = get_field("destination_image_2");
      $destination_image_3 = get_field("destination_image_3");
      $destination_image_4 = get_field("destination_image_4");
      $destination_image_5 = get_field("destination_image_5");
      $destination_image_6 = get_field("destination_image_6");    
      endwhile;
    endif;
?> 
<section class="popular-dist" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/icons/popular-bg.png');">
  <div class="section-header header-underline text-center">
    <h3 class="bold">Popular Destination</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_1['url'] ?>"/><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_1") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_2['url'] ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_2") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_3['url'] ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_3") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_4['url'] ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_4") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_5['url'] ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_5") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image_6['url'] ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= get_field("destination_title_6") ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
    </div>
  </div>
</section>
