<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>GreatBurma</title>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/assets/images/icons/favicon-logo.png">
    
    
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/style.min.css">
    
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&family=Poppins:wght@600;700&display=swap" rel="stylesheet">
  </head>

<body>
  <header>
    <!-- header top -->
    <div class="top-nav header-sticky">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="top-navbar">
              <div class="navbar-phone-no"><a class="top-nav-phone" href="tel:+95 9 xxxxxxxxx"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/phone-call.svg" alt=""><span>+95 9 xxxxxxxxx</span></a></div>
              <div class="language-switch"><a href="#">
                  <div class="en"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/en.svg" alt=""></div>
                </a><span>|</span><a href="#">
                  <div class="jp"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/jp.svg" alt=""></div>
                </a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light p-0" id="navbar-sticky">
      <div class="container">
        <div class="logo-section"><a class="navbar-brand" href="./index.html"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/logo.jpg" alt="banner one"></a>
          <div class="logo-test">
            <h3>Great Memories</h3>
            <p>travel & tour</p>
          </div>
        </div><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNav" aria-controls="headerNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse dsk-navbar" id="headerNav">
          <div class="navbar-nav">
            <div class="nav-item"><a href=""></a></div>
            <div class="nav-item active"><a class="nav-link bold upper" href="./index.html">Home<span class="sr-only">(current)</span></a></div>
            <div class="nav-item"><a class="nav-link bold upper" href="about.html">About</a></div>
            <div class="nav-item"><a class="nav-link bold upper" href="tour-packages.html">Tour Packages</a></div>
            <div class="nav-item"><a class="nav-link bold upper" href="customise-tour.html">Customise Tour</a></div>
            <div class="nav-item"><a class="nav-link bold upper" href="contactus.html">Contact</a></div>
          </div>
        </div>
      </div>
    </nav>
  </header>