<?php
/**
 * Hide editor on specific pages.
 *
 */
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  // Hide the editor on the page titled 'Homepage'
  $page = get_post_field( "post_name", $post_id,"raw");
  if($page == 'media-network' || $page == 'our-client' || $page == 'site-setting' || $page == 'service' || $page == 'about-us' || $page == 'our-work' || $page == 'career' || $page == 'contact'){ 
    remove_post_type_support('page', 'editor');
  }
  
}
?>