<?php
    get_header();
    
    if(have_posts()):
    while(have_posts()):
        the_post(); 
        $banner_image = get_field("banner_image");
        $banner_image_mb = get_field("banner_image_mb");
        $feature_image = get_field("feature_image");
        endwhile;
    endif;
?>
<section class="banner-section pad-0">
    <div class="otherbanner-desk"><img class="w-100" src="<?= $banner_image['url'] ?>" alt="banner one"></div>
    <div class="otherbanner-mb"><img class="w-100" src="<?= $banner_image_mb['url'] ?>" alt="banner one"></div>
</section>

  <div class="section-header header-underline text-center">
    <h3 class="bold"><?= __("who_are", "greatburma") ?></h3>
  </div>
  <div class="about-section">
    <div class="container">
      <div class="row staff-row">
        <div class="col-md-6">
          <h4 class="bold"><?= get_field("about_title") ?></h4>
          <p class="staff-desc"><?= get_field("about_description") ?></p>
          
        </div>
        <div class="col-md-6">
          <h4 class="bold upper opacity-0">Our Story</h4>
          <div class="img-shadow waves-effect waves-light"><img class="img-fluid" src="<?= $feature_image['url'] ?>" alt=""></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="our-vision"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/idea1.svg" alt="banner one">
            <h4 class="bold"><?= __("our_vision", "greatburma") ?></h4>
            <p><?= get_field("vision_description") ?></p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="our-mission"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/icons/rocket1.svg" alt="banner one">
            <h4 class="bold"><?= __("our_mission", "greatburma") ?></h4>
            <p><?= get_field("mission_description") ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php     
    get_footer();
?>