<?php
    get_header();
    if(have_posts()):
      while(have_posts()):
          the_post(); 
          $banner_image = get_field("banner_image");
          $banner_image_mb = get_field("banner_image_mb");
          endwhile;
      endif;
?>
<section class="banner-section pad-0">
  <div class="otherbanner-desk"><img class="w-100" src="<?= $banner_image['url'] ?>" alt="banner one"></div>
  <div class="otherbanner-mb"><img class="w-100" src="<?= $banner_image_mb['url'] ?>" alt="banner one"></div>
</section>
<div class="section-header header-underline text-center pt-0">
  <h3 class="bold"><?= __("Customised_Booking", "greatburma") ?></h3>
</div>
<section class="cutomised-section pt-0">
  <div class="container">
    <div class="row staff-row">
      <div class="col-md-12"></div>
      <div class="col-md-7">
        <form>
          <div class="form-group"><label><?= __("name", "greatburma") ?></label><input class="form-control" type="text"></div>
          <div class="form-group"><label><?= __("email", "greatburma") ?></label><input class="form-control" type="email"></div>
          <div class="form-group"><label><?= __("country", "greatburma") ?></label><input class="form-control" type="text"></div>
          <div class="form-group"><label><?= __("phone_number", "greatburma") ?></label><input class="form-control" type="text"></div>
          <div class="form-group arrival-date">
          <style>
            
            .cutomised-section .arrival-date input {
                background: url("<?php bloginfo('template_url'); ?>/assets/images/icons/calendar.svg") no-repeat right/10%;
                background-size: 50px 20px;
            }
            
          </style>
            <label><?= __("expected_arrival_date", "greatburma") ?></label>
            <input class="form-control" id="datepicker" type="text" autocomplete="off" placeholder="MM / DD / YY">
          </div>
          <div class="form-group"><label><?= __("number_of_persons", "greatburma") ?></label><input class="form-control" type="number"></div>
          <div class="form-group socical-icn"><label><?= __("meals", "greatburma") ?></label><input type="radio" name="gender" value="male" checked>Yes<input type="radio" name="gender" value="male">No</div>
          <div class="form-group socical-icn"><label><?= __("vegeterian", "greatburma") ?></label><input type="radio" name="food" value="male" checked>Yes<input type="radio" name="food" value="male">No</div>
          <div class="form-group"><label><?= __("special_requirement", "greatburma") ?></label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group">
            <div class="laguage-guide"><label><?= __("socail_media", "greatburma") ?></label>
              <div class="check-btn">
                <div class="form-check"><input class="form-check-input" id="socialCheck1" type="checkbox"><label class="form-check-label" for="socialCheck1">Facebook</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck2" type="checkbox"><label class="form-check-label" for="socialCheck2">Instagram</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck3" type="checkbox"><label class="form-check-label" for="socialCheck3">Twitter</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck4" type="checkbox"><label class="form-check-label" for="socialCheck4">Viber</label></div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-5 second-col">
        <form>
          <div class="form-group"><label><?= __("need_peference", "greatburma") ?></label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group"><label><?= __("budget", "greatburma") ?></label><input class="form-control" type="text"></div>
          <div class="form-group"><label><?= __("Number of Travel Days", "greatburma") ?></label><input class="form-control" type="number"></div>
          <div class="form-group socical-icn"><label><?= __("fee", "greatburma") ?></label><input type="radio" name="travel" value="male" checked><?= __("yes", "greatburma") ?><input type="radio" name="travel" value="male"><?= __("no", "greatburma") ?></div>
          <div class="form-group"><label><?= __("destination", "greatburma") ?></label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group"><label><?= __("message", "greatburma") ?></label><textarea class="form-control" rows="3"></textarea></div>
          <div class="form-group">
            <div class="laguage-guide"><label><?= __("language_guide", "greatburma") ?></label>
              <div class="form-check"><input class="form-check-input" id="laguageCheck1" type="checkbox"><label class="form-check-label" for="laguageCheck1"><?= __("english", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck2" type="checkbox"><label class="form-check-label" for="exampleCheck2"><?= __("chinese", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck3" type="checkbox"><label class="form-check-label" for="exampleCheck3"><?= __("french", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck4" type="checkbox"><label class="form-check-label" for="exampleCheck4"><?= __("german", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck5" type="checkbox"><label class="form-check-label" for="exampleCheck5"><?= __("italian", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck6" type="checkbox"><label class="form-check-label" for="exampleCheck6"><?= __("japanese", "greatburma") ?></label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck7" type="checkbox"><label class="form-check-label" for="exampleCheck7"><?= __("spanish", "greatburma") ?></label></div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-12">
        <div class="booking-formbtn text-center"><a href="#!"><button class="btn btn-primary md-form waves-effect waves-light text-center"><?= __("book_now", "greatburma") ?></button></a></div>
      </div>
    </div>
  </div>
</section>
<?php     
    get_footer();
?>