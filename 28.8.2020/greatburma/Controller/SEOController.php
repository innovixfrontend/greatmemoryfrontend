<?php
function GetSEO(){
  $seo_title = "Myanmar Ganad Advertising Co., Ltd.";
  $seo_description = "We being the first international outdoor company to enter Myanmar in 1996 allowed fast dominance of the local outdoor advertising landscape. The Yangon office oversees more than 100 billboards, LED sites and bus shelter network in the country.With Myanmar now rapidly opening up to foreign investment, we are poised to help new market entrants quickly build their brand presence.";
  $img = projectfolder()."/assets/images/icons/logo.png";
    if(is_page() || is_singular('careers'))
    {
      if(have_posts()){
        while (have_posts()) {
          the_post();

          if(get_field("seo_image")){
            $img = GetImage("seo_image","large");
          }
          if (!empty(get_field("seo_title")))
          {
            $seo_title = get_field("seo_title");
          }
          if (!empty(get_field("seo_description")))
          {
            $seo_description = get_field("seo_description");
          }

        } 
        wp_reset_postdata();
      }
    }
    else if(is_home('')){
      $query = new WP_Query('pagename=media-network');
      if($query->have_posts()){
          while ($query->have_posts()) {
          $query->the_post();

          if(get_field("seo_image")){
            $img = GetImage("seo_image","large");
          }
          if (!empty(get_field("seo_title")))
          {
            $seo_title = get_field("seo_title");
          }
          if (!empty(get_field("seo_description")))
          {
            $seo_description = get_field("seo_description");
          }

          } 
      }
    }
    
  ?>
    <meta name="keywords" content="<?= $seo_title ?>" />
    <meta name="description" content="<?= $seo_description ?>" />
    <meta property="og:title" content="<?= $seo_title ?>">
    <meta property="og:site_name" content="Myanmar Ganad Advertising Co., Ltd.">
    <meta property="og:description" content="<?= $seo_description ?>">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?= $img ?>">
    <meta property="og:url" content="<?= current_location() ?>">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="<?= $seo_description ?>" />
    <meta name="twitter:title" content="Myanmar Ganad Advertising Co., Ltd." />
<?php
}
?>