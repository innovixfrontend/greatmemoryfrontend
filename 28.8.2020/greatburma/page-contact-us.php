<?php
    $query = new WP_Query('pagename=site-setting');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $facebook = get_field("facebook");
        $instagram = get_field("instagram");
        $twitter = get_field("twitter");
        $viber = get_field("viber");
      endwhile;
    endif;
    // end of social icon
    get_header();
    get_template_part('otherheader');
    if(have_posts()):
    while(have_posts()):
    the_post();
?>
<section class="contact-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="contact-info">
          <h3 class="bold section-header header-underline"><?= __("contact_information", "greatburma") ?></h3>
          <ul>
            <li><i class="fas fa-map-marked-alt"></i>
              <p><?= get_field("address") ?></p>
            </li>
            <li><i class="fas fa-phone-alt"></i>
              <p><?= get_field("phone_1") ?>,<?= get_field("phone_2") ?></p>
            </li>
            <li><i class="fas fa-envelope"></i>
              <p><?= get_field("email") ?></p>
            </li>
          </ul>
        </div>
       
        
        <div class="share-with-us">
          <h5 class="bold"><?= __("socially_connect", "greatburma") ?></h5>
          <div class="share-with-us-blog">
            <?php
              if (!empty($facebook)) {
            ?>
            <a class="text-center" href="<?= $facebook?>">
              <i class="fab fa-facebook-f"></i>
            </a>
            <?php
              }
              if (!empty($instagram)) {
            ?>
            <a class="text-center" href="<?= $instagram?>"> 
              <i class="fab fa-instagram"></i>
            </a>
            <?php
              }
              if (!empty($twitter)) {
            ?>
            <a class="text-center" href="<?= $twitter?>">
              <i class="fab fa-twitter"></i>
            </a>
            <?php
              }
              if (!empty($viber)) {
            ?>
            <a class="text-center" href="viber://chat?number=<?= $viber?>"> 
              <i class="fab fa-viber"></i>
            </a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="map-wrap"><?= get_field("location_map") ?></div>
      </div>
    </div>
  </div>
</section>
<?php 
  endwhile;
  endif; 
  get_footer();
?>