<?php
    $query = new WP_Query('pagename=site-setting');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $facebook = get_field("facebook");
        $instagram = get_field("instagram");
        $twitter = get_field("twitter");
        $viber = get_field("viber");
        $logo = get_field("logo");
      endwhile;
    endif;
    // end of social icon
    $query = new WP_Query('pagename=contact-us');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $phone_1 = get_field("phone_1");
        $email = get_field("email");
        $address = get_field("address");
        
      endwhile;
    endif;
?>
  <section class="return-to-top-section pad-0"><a class="return-to-top btn-floating waves-effect waves-light" href="javascript:"><i class="fas fa-arrow-up"> </i></a></section>
  <section class="float-contact-us pad-0">
    <div class="contact-us"><a class="contact-us-btn share_btn"><i class="fas fa-share-alt"></i></a></div>
    <div class="social-icns d-animate">
      <div id="shareBlock"></div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4 email">
          <div class="footer-logo"> <img src="<?= $logo['url'] ?>" alt="">
            <div class="logo-text">
              <h3>Great Memories</h3>
              <p>travel & tour </p>
            </div>
          </div>

          <div class="share-with-us-blog">
            <?php
              if (!empty($facebook)) {
            ?>
            <a class="text-center" href="<?= $facebook?>">
              <i class="fab fa-facebook-f"></i>
            </a>
            <?php
              }
              if (!empty($instagram)) {
            ?>
            <a class="text-center" href="<?= $instagram?>"> 
              <i class="fab fa-instagram"></i>
            </a>
            <?php
              }
              if (!empty($twitter)) {
            ?>
            <a class="text-center" href="<?= $twitter?>">
              <i class="fab fa-twitter"></i>
            </a>
            <?php
              }
              if (!empty($viber)) {
            ?>
            <a class="text-center" href="viber://chat?number=<?= $viber?>"> 
              <i class="fab fa-viber"></i>
            </a>
            <?php } ?>
          </div>
        </div>
        <div class="col-md-4 address">
          <h3><?= __("links", "greatburma") ?></h3>
          <ul>
            <li> <a href="<?= home_url('./') ?>"><?= __("home", "greatburma") ?></a></li>
            <li> <a href="<?= home_url('./about-us'); ?>"><?= __("about", "greatburma") ?> </a></li>
            <li> <a href="<?= home_url('./tour-package'); ?>"><?= __("tour_packages", "greatburma") ?></a></li>
            <li> <a href="<?= home_url('./customize-tours'); ?>"><?= __("customise_tour", "greatburma") ?></a></li>
            <li> <a href="<?= home_url('./contact-us'); ?>"><?= __("contact", "greatburma") ?> </a></li>
          </ul>
        </div>
        <div class="col-md-4 ph-number">
          <h3><?= __("links", "greatburma") ?></h3>
          <div class="contact-phone d-flex">
            <p class="bold"><?= __("phone_number", "greatburma") ?></p><a href="tel:<?= $phone_1?>"><?= $phone_1?></a>
          </div>
          <div class="contact-mail d-flex">
            <p class="bold"><?= __("email", "greatburma") ?> </p><a href="mailto:<?= $email?>"><?= $email?></a>
          </div>
          <div class="contact-address d-flex">
            <p class="bold"><?= __("address", "greatburma") ?></p><span class="address-txt"><?= $address?></span>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <nav class="copy-right">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <p>Copyright &copy; <script>
              new Date().getFullYear() > 2010 && document.write(new Date().getFullYear());
            </script> Great Memories Travel & Tours Co.,Ltd. All Rights Reserved. Powered by<a href="http://www.innovixdigital.com/" target="_blank">Innovix Digital</a></p>
        </div>
      </div>
    </div>
  </nav>
</body>
<script src="<?php bloginfo('template_url'); ?>/js/core.min.js"></script>
<script src="https://kit.fontawesome.com/0fdd460947.js" crossorigin="anonymous"></script>

      
      


</html>