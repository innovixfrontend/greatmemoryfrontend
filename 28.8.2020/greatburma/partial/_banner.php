<section class="banner-slide-section pad-0">
  <style>
    .banner-slide-section .banner-slide-wrapper .banner-slide .slick-next::before {
      background: url("<?php bloginfo('template_url'); ?>/assets/images/icons/arrow-right-white.svg") no-repeat center/100%;
    }
    .banner-slide-section .banner-slide-wrapper .banner-slide .slick-prev::before {
      background: url("<?php bloginfo('template_url'); ?>/assets/images/icons/arrow-right-white.svg") no-repeat center/100%;
    }
  </style>
  <div class="banner-slide-wrapper">
    <div class="banner-slide">
      <div class="slide-wrap">
      <div class="slide-item"><img class="w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/bagan-img.png" alt="banner one"></div>
      </div>
      <div class="slide-wrap">
      <div class="slide-item"><img class="w-100" src="<?php bloginfo('template_url'); ?>/assets/images/icons/bagan-img.png" alt="banner one"></div>
      </div>
    </div>
  </div>
</section>

