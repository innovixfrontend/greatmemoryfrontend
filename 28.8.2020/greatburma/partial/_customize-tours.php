<?php
    
    
    $query = new WP_Query('pagename=customize-tours');
    if($query->have_posts()):
    while($query->have_posts()):
      $query->the_post(); 
      $feature_image_1 = get_field("feature_image_1");
      $feature_image_2 = get_field("feature_image_2");
      $feature_image_3 = get_field("feature_image_3");  
        endwhile;
    endif;
?>
<section class="holiday-package">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3 class="bold section-header header-underline"><?= __("customise_tour", "greatburma") ?></h3>
          <p><?= nl2br(get_field("customize_description")) ?></p>
          <div class="pagin-load-more dsk-view"><a href="<?= home_url('./customize-tours'); ?>"><button class="btn btn-primary book-now"><?= __("read_more", "greatburma") ?></button></a></div>
        </div>
        <div class="col-md-6">
          <div class="cus-tour" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/icons/bg-layer.png');background-repeat: no-repeat;background-position: center;"> <img class="layerone" src="<?= $feature_image_1['url'] ?>" alt="">
            <div><img class="layertwo" src="<?= $feature_image_2['url'] ?>" alt=""></div>
            <div><img class="layerthree" src="<?= $feature_image_3['url'] ?>" alt=""></div>
          </div>
        </div>
      </div>
    </div>
</section>
