<?php
    get_header();
    if(have_posts()):
      while(have_posts()):
          the_post(); 
          $banner_image = get_field("banner_image");
          $banner_image_mb = get_field("banner_image_mb");
          endwhile;
      endif;
?>
<section class="banner-section pad-0">
  <div class="otherbanner-desk"><img class="w-100" src="<?= $banner_image['url'] ?>" alt="banner one"></div>
  <div class="otherbanner-mb"><img class="w-100" src="<?= $banner_image_mb['url'] ?>" alt="banner one"></div>
</section>
<div class="section-header header-underline text-center pt-0">
  <h3 class="bold">Customised Tour Booking</h3>
</div>
<section class="cutomised-section pt-0">
  <div class="container">
    <div class="row staff-row">
      <div class="col-md-12"></div>
      <div class="col-md-7">
        <form>
          <div class="form-group"><label>Name</label><input class="form-control" type="text"></div>
          <div class="form-group"><label>Email</label><input class="form-control" type="email"></div>
          <div class="form-group"><label>Country</label><input class="form-control" type="text"></div>
          <div class="form-group"><label>Phone Number</label><input class="form-control" type="text"></div>
          <div class="form-group arrival-date"><label>Expected Arrival Date</label><input class="form-control" id="datepicker" type="text" autocomplete="off" placeholder="MM / DD / YY"></div>
          <div class="form-group"><label>Number of Persons </label><input class="form-control" type="number"></div>
          <div class="form-group socical-icn"><label>Including Meals</label><input type="radio" name="gender" value="male" checked>Yes<input type="radio" name="gender" value="male">No</div>
          <div class="form-group socical-icn"><label>Vegetarian</label><input type="radio" name="food" value="male" checked>Yes<input type="radio" name="food" value="male">No</div>
          <div class="form-group"><label>Special Requirement </label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group">
            <div class="laguage-guide"><label>Social Media</label>
              <div class="check-btn">
                <div class="form-check"><input class="form-check-input" id="socialCheck1" type="checkbox"><label class="form-check-label" for="socialCheck1">Facebook</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck2" type="checkbox"><label class="form-check-label" for="socialCheck2">Instagram</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck3" type="checkbox"><label class="form-check-label" for="socialCheck3">Twitter</label></div>
                <div class="form-check"><input class="form-check-input" id="socialCheck4" type="checkbox"><label class="form-check-label" for="socialCheck4">Viber</label></div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-5 second-col">
        <form>
          <div class="form-group"><label>Need & Peference</label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group"><label>Estimate Budget</label><input class="form-control" type="text"></div>
          <div class="form-group"><label>Number of Travel Days</label><input class="form-control" type="number"></div>
          <div class="form-group socical-icn"><label>Including Entrance Fees </label><input type="radio" name="travel" value="male" checked>Yes<input type="radio" name="travel" value="male">No</div>
          <div class="form-group"><label>Destination</label><textarea class="form-control" rows="2"></textarea></div>
          <div class="form-group"><label>Message</label><textarea class="form-control" rows="3"></textarea></div>
          <div class="form-group">
            <div class="laguage-guide"><label>Language Guide</label>
              <div class="form-check"><input class="form-check-input" id="laguageCheck1" type="checkbox"><label class="form-check-label" for="laguageCheck1">English</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck2" type="checkbox"><label class="form-check-label" for="exampleCheck2">Chinese</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck3" type="checkbox"><label class="form-check-label" for="exampleCheck3">Frech</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck4" type="checkbox"><label class="form-check-label" for="exampleCheck4">German</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck5" type="checkbox"><label class="form-check-label" for="exampleCheck5">Italian</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck6" type="checkbox"><label class="form-check-label" for="exampleCheck6">Japanese</label></div>
              <div class="form-check"><input class="form-check-input" id="exampleCheck7" type="checkbox"><label class="form-check-label" for="exampleCheck7">Spanish</label></div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-12">
        <div class="booking-formbtn text-center"><a href="#!"><button class="btn btn-primary md-form waves-effect waves-light text-center">Book Now </button></a></div>
      </div>
    </div>
  </div>
</section>
<?php     
    get_footer();
?>