<?php 
// start multi language
function theme_init() {
  load_theme_textdomain('greatburma', get_template_directory() . '/translate');
}
add_action('init', 'theme_init');
define('WPLANG', ' en_US');
// end multi language
?>