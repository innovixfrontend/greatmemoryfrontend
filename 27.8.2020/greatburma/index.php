<?php
    get_header();
    get_template_part('partial/_banner');
    get_template_part('partial/_latest-tour-packages');
    get_template_part('partial/_customize-tours');
    get_template_part('partial/_popular-destination');
    get_footer(); 
?>