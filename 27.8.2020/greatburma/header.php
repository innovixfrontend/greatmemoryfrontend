<?php
    $query = new WP_Query('pagename=site-setting');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        
        $logo = get_field("logo");
      endwhile;
    endif;
    // end of social icon
    $query = new WP_Query('pagename=contact-us');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $phone_1 = get_field("phone_1");
        
        
      endwhile;
    endif;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>GreatBurma</title>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/assets/images/icons/favicon-logo.png">
    
    
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/style.min.css">
    
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@500&family=Poppins:wght@600;700&display=swap" rel="stylesheet">
  </head>

<body>
<header>
    <!-- header top -->
    <div class="top-nav header-sticky">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="top-navbar">
              <div class="navbar-phone-no"><a class="top-nav-phone" href="tel:<?= $phone_1?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/phone-call.svg" alt=""><span><?= $phone_1 ?></span></a></div>
              <div class="language-switch"><a href="#">
                  <div class="en"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/en.svg" alt=""></div>
                </a><span>|</span><a href="#">
                  <div class="jp"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/jp.svg" alt=""></div>
                </a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light p-0" id="navbar-sticky">
      <div class="container">
        <div class="logo-section"><a class="navbar-brand" href="./index.html"><img class="img-fluid" src="<?= $logo['url'] ?>" alt="banner one"></a><a class="logo-test" href="./index.html">
            <h3>Great Memories</h3>
            <p>travel & tour</p>
          </a></div><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNav" aria-controls="headerNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse dsk-navbar" id="headerNav">
          <div class="navbar-nav">
            <div class="nav-item"><a href=""></a></div>
            <div class="nav-item <?= (is_home('')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./') ?>"><?= __("home", "greatburma") ?><span class="sr-only">(current)</span></a></div>            
            <div class="nav-item <?= (is_page('about-us') || is_singular('about-us')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./about-us'); ?>"><?= __("about", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('tour-package') || is_singular('tour-package')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./tour-package'); ?>"><?= __("tour_packages", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('customize-tours') || is_singular('customize-tours')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./customize-tours'); ?>"><?= __("customise_tour", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('contact-us') || is_singular('contact-us')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./contact-us'); ?>"><?= __("contact", "greatburma") ?></a></div>
          </div>
        </div>
      </div>
    </nav>
  </header>