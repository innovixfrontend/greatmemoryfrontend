<?php
    $query = new WP_Query('pagename=site-setting');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $seo_title=get_field("seo_title");
        $seo_description=get_field("seo_title");
        $logo = get_field("logo");
      endwhile;
    endif;
    // end of social icon
    $query = new WP_Query('pagename=contact-us');
    if($query->have_posts()):
      while($query->have_posts()):
        $query->the_post(); 
        $phone_1 = get_field("phone_1");
        
        
      endwhile;
    endif;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- meta tags -->
    <meta name="keywords" content="Media Network" />
    <meta name="description" content="<?php= $seo_description ?>"/>
    <meta property="og:title" content="<?php= $seo_title ?>">
    <meta property="og:site_name" content="Great Memories Travel & Tours Co.,Ltd.">
    <meta property="og:description" content="<?php= $seo_description ?>"/>
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?= $logo['url'] ?>">
    <meta property="og:url" content="<?= current_location()?>">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="The leading provider of out-of-home advertising solutions in Myanmar for 25 years. Ganad believes in working together with clients to develop comprehensive Out of Home strategies that suit all marketing objectives."/>
    <meta name="twitter:title" content="Myanmar Ganad Advertising Co., Ltd." />
    <!-- end of meta tags -->
    <title>Great Burma</title>
    <link rel="icon" href="<?php bloginfo('template_url'); ?>/assets/images/icons/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <!-- for sub header -->
    <link href="https://fonts.googleapis.com/css?family=Oleo+Script&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Oleo+Script:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style/style.min.css">
    
    
  </head>

<body>
<header>
    <!-- header top -->
    <div class="top-nav header-sticky">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="top-navbar">
              <div class="navbar-phone-no"><a class="top-nav-phone" href="tel:<?= $phone_1?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/icons/phone-call.svg" alt=""><span><?= $phone_1 ?></span></a></div>
              <div class="language-switch">
                <a href="<?= wpm_translate_url("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], "en"); ?>">
                  <div class="en"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/en.svg" alt=""></div>
                </a>
                <span>|</span>
                <a href="<?= wpm_translate_url("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], "ja"); ?>">
                  <div class="jp"> <img src="<?php bloginfo('template_url'); ?>/assets/images/icons/jp.svg" alt=""></div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light p-0" id="navbar-sticky">
      <div class="container">
        <div class="logo-section"><a class="navbar-brand" href="./index.html"><img class="img-fluid" src="<?= $logo['url'] ?>" alt="banner one"></a><a class="logo-test" href="./index.html">
            <h3>Great Memories</h3>
            <p>travel & tour</p>
          </a></div><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerNav" aria-controls="headerNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse dsk-navbar" id="headerNav">
          <div class="navbar-nav">
            <div class="nav-item"><a href=""></a></div>
            <div class="nav-item <?= (is_home('')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./') ?>"><?= __("home", "greatburma") ?><span class="sr-only">(current)</span></a></div>            
            <div class="nav-item <?= (is_page('about-us')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./about-us'); ?>"><?= __("about", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('tour-package') || is_singular('tour-packages')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./tour-package'); ?>"><?= __("tour_packages", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('customize-tours')) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./customize-tours'); ?>"><?= __("customise_tour", "greatburma") ?></a></div>
            <div class="nav-item <?= (is_page('contact-us') ) ? 'active' : ''; ?>"><a class="nav-link bold upper" href="<?= home_url('./contact-us'); ?>"><?= __("contact", "greatburma") ?></a></div>
          </div>
        </div>
      </div>
    </nav>
  </header>