<?php
    get_header();
    
    if(have_posts()):
    while(have_posts()):
        the_post(); 
        $banner_image = get_field("banner_image");
        $banner_image_mb = get_field("banner_image_mb");
        endwhile;
    endif;
?>
  <section class="banner-section pad-0">
    <div class="otherbanner-desk"><img class="w-100" src="<?= $banner_image['url'] ?>" alt="banner one"></div>
    <div class="otherbanner-mb"><img class="w-100" src="<?= $banner_image_mb['url'] ?>" alt="banner one"></div>
  </section>
  <section class="section-header header-underline text-center">
    <h3 class="bold"><?= __("tour_booking", "greatburma") ?></h3>
  </section>
  <?php get_template_part('partial/_tour-package');?>
<?php     
  get_footer();
?>