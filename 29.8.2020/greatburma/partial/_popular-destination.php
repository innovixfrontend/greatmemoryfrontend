
<section class="popular-list">
  <div class="section-header header-underline text-center">
    <h3 class="bold"><?= __("popular_destination", "greatburma") ?></h3>
  </div>
  <div class="container">
    <div class="row">
      <?php 
        $args = [
          'post_type' => 'popular-destination',
          'posts_per_page' => -1,
        ];
        $tour_packages=new WP_Query($args);
        if($tour_packages->have_posts()){
          while($tour_packages->have_posts()) {
            $tour_packages->the_post();
            $destination_title = get_field("destination_title");
            
            $destination_image = GetImage("destination_image","large");

      ?>
      <div class="col-md-4 popular-card">
        <div class="card no-card">
          <div class="view overlay"><img class="card-img-top" src="<?= $destination_image ?>" alt="banner one"><a href="#!">
              <div class="mask rgba-black-slight">
                <div class="card-overlay-txt">
                  <h4 class="bold card-title"><?= $destination_title ?></h4>
                </div>
              </div>
            </a></div>
        </div>
      </div>
      <?php 
          }
        }
      ?>
      
    </div>
  </div>
</section>
