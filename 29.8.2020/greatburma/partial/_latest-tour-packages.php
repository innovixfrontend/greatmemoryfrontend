<section class="latest-tours">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-header header-underline text-center">
            <h3 class="bold"><?= __("latest_packages", "greatburma") ?></h3>
          </div>
        </div>
      </div>
      <div class="row tour-card-wrapper">
      <?php 
        $args = [
          'post_type' => 'tour-packages',
          'posts_per_page' => 4,
        ];
        $tour_packages=new WP_Query($args);
        if($tour_packages->have_posts()){
          while($tour_packages->have_posts()) {
            $tour_packages->the_post();      
            $feature_image = acf_photo_gallery('feature_image', $post->ID);

      ?>
      <div class="col-md-6 tour-card">
        <div class="card"><a href="<?=the_permalink() ?>">
            <?php 
              foreach($feature_image as $img1):

            ?>
            <div class="view overlay"><img class="card-img-top w-100" src="<?= $img1['full_image_url']; ?>" alt="banner one">
            
              <div class="mask rgba-white-slight"> </div>
            </div>
            <?php 
              break;
              endforeach;
            ?>
            <div class="card-body">
              <h4 class="bold"><?= get_field("tour_title") ?></h4>
              <div class="card-text">
                <div class="duration"><i class="fas fa-history"></i>
                  <p><?= get_field("duration") ?></p>
                </div>
                <div class="price">
                  <h4><?= get_field("price") ?></h4>
                </div>
              </div>
              <div class="button-read text-right">
                <p class="upper bold"><?= __("read_more", "greatburma") ?></p>
              </div>
            </div>
          </a></div>
      </div>
        <?php 
          }
        }
      ?>
      </div>
    </div>
  </section>