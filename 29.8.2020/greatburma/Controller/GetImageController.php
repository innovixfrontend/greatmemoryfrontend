<?php 
if(!function_exists('GetImage')){
  function GetImage($name,$size)
  {
    $img_url = get_field($name);
    $img_path = $img_url["sizes"][$size];
    return $img_path;
  }
}
  
?>